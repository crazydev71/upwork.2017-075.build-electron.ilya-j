# scope
This project includes a simplified application (.src/empty-1) that contains all the relevant components of a bigger app being developed.

It will allow a simplified testbed for the automation of testing, build and deployment processes.

Please assume that any point of the spec can be revisited in search for a better, more convenient or more relevant solution. Constructive discussion is very welcomed.


# main objectives
1. setup automated testing on the EMPTY-1 app
   * testing frameworks/packages to setup
     * protractor
     * karma
     * jasmine
     * spectron (see https://github.com/electron/spectron/issues/164)
   * create a relevant example (ie, usefull) of the use of each one of the tools
   * include the testing as part of the build process

2. setup automated build process for the EMPTY-1 app
   * build targets:
     * all 3 major platforms (Windows, Mac, Linux)
     * allow build all or individually
   * include support to generate semver build numbers based on tags
   * configure a dockerized build process to run in one of the following:
     * gitlab pipelines (preferred)
     * jenkins
     * team city
     * bitbucket pipelines
   * external runners may be used if required (AppVeyor for Windows builds, Travis CI for OSX builds); in this case, integration must also be setup;

3. setup automated upload of releases to gitlab releases
   * upon succesfull testing and building, artifacts (inst),
     upload to /releases in gitlab and/or in blob storage (S3/Azure)
